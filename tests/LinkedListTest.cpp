//
// Created by JoeMama on 16/11/2021.
//
#include "linked_list.hpp"
#include <iostream>

int main()
{
    ds::linked_list<int> list;

    list.add(1);
    list.add(2);
    list.add(7);
    list.add(69);
    list.add(32);
    list.add(16);
    list.add(9);

    std::cout << list << std::endl;

    for (int i = 0; i < list.size(); ++i) {
        list[i] = 69;
    }

    std::cout << list << std::endl;

    ds::linked_list<int> newList;

    for (int i = 0; i < 100; ++i) {
        newList.add(i);
    }

    std::cout << newList << std::endl;

    ds::linked_list<std::string> names;

    names.add("Jonathan Joestar");
    names.add("Joseph Joestar");
    names.add("Jotaro Kujo");
    names.add("Josuke Higashikata");
    names.add("Giorno Giavanna");
    names.add("Jolyne Kujo");
    names.add("Jonnie Joestar");
    names.add("Josuke Higashikata");

    std::cout << names << std::endl;

    auto* listPtr = new ds::linked_list<int>();

    listPtr->add(12);
    listPtr->add(12);
    listPtr->add(69);
    listPtr->add(42);
    listPtr->add(18);
    listPtr->add(78);
    listPtr->add(34);
    listPtr->add(35);

    std::cout << *listPtr << std::endl;

    listPtr->pop();
    listPtr->pop();
    listPtr->pop();
    listPtr->pop();
    listPtr->pop();
    listPtr->push(69);
    std::cout << listPtr->pop() << std::endl;

    std::cout << *listPtr << std::endl;

    delete listPtr;

}
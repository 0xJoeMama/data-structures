//
// Created by JoeMama on 02/11/2021.
//

#pragma once

#include <utility>
#include <ostream>

namespace ds
{
    template<typename T>
    struct Node
    {
        T value;
        Node<T>* next;

        explicit Node(T value, Node<T>* next = nullptr) : value(std::move(value)), next(next)
        {}

        Node(Node<T>&& moving) noexcept
        {
            this->value = std::move(value);
            this->next = moving.next;
        }
    };

    template<typename T>
    class linked_list
    {
    private:
        Node<T>* head;
        uint64 count;
    public:
        linked_list() : head(nullptr), count(0)
        {}

        linked_list(const linked_list<T>& other) : count(0), head(nullptr)
        {
            auto tmp = other.head;

            while (tmp) {
                this->add(tmp->value);
                tmp = tmp->next;
            }
        }

        ~linked_list()
        {
            Node<T>* tmp = this->head;
            Node<T>* toDelete;
            int deletedElems = 0;

            while (tmp) {
                toDelete = tmp;
                tmp = tmp->next;

                delete toDelete;
                deletedElems++;
            }
//            std::cout << "Successfully deleted " << deletedElems << " elements." << std::endl;
        }

        T& add(const T& ref)
        {
            count++;
            if (!head) {
                init_head(ref);

                return head->value;
            } else {
                auto* newNode = new Node<T>(ref, nullptr);
                Node<T>* end = get_tail_node();

                end->next = newNode;
                return newNode->value;
            }
        }

        T& push(const T& ref)
        {
            auto* newHead = new Node<T>(ref, head);
            head = newHead;
            count++;

            return newHead->value;
        }

        T pop()
        {
            auto* nextHead = head->next;
            T value = head->value;
            delete head;
            head = nextHead;
            count--;

            return value;
        }

        [[nodiscard]] const uint64& size() const
        {
            return this->count;
        }

        friend std::ostream& operator<<(std::ostream& ostream, const linked_list& list)
        {
            ostream << "[";

            for (int i = 0; i < list.count; ++i) {
                if (i != 0) {
                    ostream << ", ";
                }
                ostream << list[i];
            }

            ostream << "]";

            return ostream;
        }

        T& operator[](const uint64& index) const
        {
            // TODO: Find the correct way to manage debug.
#ifdef _GLIBCXX_DEBUG_ONLY
            if (index > this->size()) {
                throw std::invalid_argument("Invalid array access.");
            }
#endif // _GLIBCXX_DEBUG_ONLY
            uint64 currentIndex = index;
            Node<T>* currentNode = this->head;

            while (currentIndex) {
                currentIndex--;
                currentNode = currentNode->next;
            }

            return currentNode->value;
        }

        bool operator==(const linked_list& rhs) const
        {
            return head == rhs.head &&
                   count == rhs.count;
        }

    private:
        void init_head(const T& value)
        {
            head = new Node<T>(value, nullptr);
        }

        Node<T>* get_tail_node()
        {
            Node<T>* here = this->head;

            while (here->next) {
                here = here->next;
            }

            return here;
        }
    };
}
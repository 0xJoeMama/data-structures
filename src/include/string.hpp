//
// Created by JoeMama on 16/11/2021.
//

#pragma once

#include <iostream>

namespace ds
{
    typedef unsigned long long uint64;

    class string
    {
    private:
        const char* buffer;
        uint64 _length;

    public:
        string(const char* literal); // NOLINT(google-explicit-constructor) We do want to use implicit instantiation here.

//        template<uint64 SIZE>
//        string(const char literal[SIZE]) // NOLINT(google-explicit-constructor) Same here
//        {
//            std::cout << "Stack allocation" << std::endl;
//            _length = SIZE - 1;
//            char buf[SIZE];
//            buffer = &buf;
//
//            memcpy(buffer, literal, _length);
//        }

        ~string();

        const char& operator[](uint64 index) const;

        [[nodiscard]] uint64 length() const;

        [[nodiscard]] const char* ptr() const;

        friend std::ostream& operator<<(std::ostream& ostream, const string& string);

        bool operator==(const string& rhs) const;

        bool operator!=(const string& rhs) const;

        static uint64 get_length(const char* literal);
    };
}
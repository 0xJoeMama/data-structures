//
// Created by JoeMama on 01/10/2021.
//

#include "string.hpp"

namespace ds
{
    string::string(const char* literal)
    {
        _length = get_length(literal);
        buffer = new char[_length];
        // TODO: Look into this.
        // Evil const evasion
        memcpy((void*) buffer, literal, _length);
    }

    string::~string()
    {
        delete[] buffer;
    }

    const char& string::operator[](uint64 index) const
    {
        // TODO: Add indexing checks for debug mode.
        return buffer[index];
    }

    uint64 string::length() const
    {
        return _length;
    }

    const char* string::ptr() const
    {
        return buffer;
    }

    std::ostream& operator<<(std::ostream& ostream, const string& string)
    {
        for (uint64 i = 0; i < string._length; ++i) {
            ostream << string[i];
        }

        return ostream;
    }

    bool string::operator==(const string& rhs) const
    {
        if (rhs._length != _length) {
            return false;
        }

        for (uint64 i = 0; i < _length; ++i) {
            if ((*this)[i] != rhs[i]) {
                return false;
            }
        }

        return true;
    }

    bool string::operator!=(const string& rhs) const
    {
        return !(rhs == *this);
    }

    uint64 string::get_length(const char* literal)
    {
        uint64 ret = 0;

        while (literal[ret] != '\0') {
            ret++;
        }

        return ret;
    }

}
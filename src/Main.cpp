#include <iostream>
#include "string.hpp"
#include "linked_list.hpp"
#include <chrono>

using namespace std::chrono;
typedef time_point<system_clock, duration<uint64_t, std::nano>> time_point_ns;

int main()
{
    time_point_ns begin = system_clock::now();
    ds::string s = "Hello there";

    std::cout << s << std::endl;

    auto* ptr = s.ptr();

    ((char*) ptr)[0] = 69;

    std::cout << s << std::endl;

    for (int i = 0; i < s.length(); ++i) {
        std::cout << ptr[i] << std::endl;
    }

    std::cout << "===========" << std::endl;

    ds::string s2 = "Hello there";

    std::cout << ((s2 == s) ? "true" : "false") << std::endl;
    time_point_ns end = system_clock::now();

    std::cout << (end - begin).count() / 1000000 << "ms" << std::endl;

    ds::linked_list<int> list;
    list.push(1);
    list.push(2);
    list.push(3);
    list.push(4);
    list.push(5);
    list.push(6);
    std::cout << list << std::endl;

    return 0;
}
